
# MERN Chat App

MERN Chat App is a Full Stack Chatting App.
Uses Socket.io for real time communication and stores user details in encrypted format in Mongo DB Database.
## Tech Stack

**Client:** React JS

**Server:** Node JS, Express JS

**Database:** Mongo DB
  
## Demo

## Run Locally

Clone the project


Go to the project directory

```bash
  cd MERN_Chat
```

Install dependencies

```bash
  npm install
```

```bash
  cd frontend/
  npm install
```

Start the server

```bash
  npm start
```
Start the Client

```bash
  cd frontend
  npm start
```

If you have some issues with Chat Page, you can use Postman to create some chats:

1) Make a Login request with POST http://localhost:5000/api/user/login with body { "email":"", "password": ""} and save the token from response body. 
Example: 
```bash
curl —location —request POST 'http://localhost:5000/api/user/login' \
--header 'Content-Type: application/json' \
--data-raw '{
"email": "bbb.bbb@gmail.com",
"password": "111"
}'
```
2) For creating a One on one chat: in Authorization choose Type Bearer, entry your saved token in the field "Token", make POST request on http://localhost:5000/api/chat with body { "userId": ""}.
Example: 
```bash
curl —location —request POST 'http://localhost:5000/api/chat' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYzcwNGM5NzhjYTU4YWUwZjE0ZTNkOCIsImlhdCI6MTY0MDUyMzExOSwiZXhwIjoxNjQzMTE1MTE5fQ.NRSFN1QRwDv86tISVxetIvAPfQN_aN7NMbw6MQCNJbk' \
--header 'Content-Type: application/json' \
--data-raw '{
"userId": "61c7928cb9390edfaa3df788"
}
```
  For creating a Group chat: in Authorization choose Type Bearer, entry your saved token in the field "Token", make POST request on http://localhost:5000/api/chat/group with body { "name": "", "users": "[]"}.
Example:
```bash
curl —location —request POST 'http://localhost:5000/api/chat/group' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYzcwNGM5NzhjYTU4YWUwZjE0ZTNkOCIsImlhdCI6MTY0MDUyMzExOSwiZXhwIjoxNjQzMTE1MTE5fQ.NRSFN1QRwDv86tISVxetIvAPfQN_aN7NMbw6MQCNJbk' \
--header 'Content-Type: application/json' \
--data-raw '{
"name": "Test Group",
"users": "[\"61c7928cb9390edfaa3df788\", \"61c7915fb9390edfaa3df785\"]"
}'
```
All API requests can be found in files of the directory "./backend/routes/".


  
